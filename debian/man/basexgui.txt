NAME
  basexgui - XML database system and XPath/XQuery processor (graphical mode)

SYNOPSIS
  basexgui [-d] [file]

OPTIONS
  A short description of option can be obtained from

    $ basexgui \-h

  or by browsing https://docs.basex.org/wiki/Command-Line_Options#GUI

DESCRIPTION
  basexgui starts a standalone locally running instance of the BaseX database system.

  In addition to the main database functionality of basex(1), the graphical mode offers innovative graphical frontends to visualize XML data.
  An advanced embedded XQuery Editor is coupled to the visualizations, providing instant result feedback.

  XML and XQuery files can be passed on as parameters. If an XML file is passed, a database instance is created from this file. If an XQuery file is passed on, it is opened in the XQuery editor.

SEE ALSO
  basex(1), basexserver(1), basexclient(1)

   .basexhome                can be created by a user to mark a folder as home directory.
   .basex                    BaseX (standalone and server) properties
   .basexgui                 BaseX additional GUI properties 
   .basexhistory             contains commands that have been typed in most recently.
   ${basexhome}/data         Default database directory
   ${basexhome}/data/.logs   Server logs
   ${basexhome}/repo         Package repository

  BaseX Documentation Wiki: https://docs.basex.org

HISTORY
  BaseX started as a research project of the Database and Information Systems
  Group (DBIS) at the University of Konstanz in 2005 and soon turned into a
  feature-rich open source XML database and XPath/XQuery processor.
 
LICENSE
  New (3-clause) BSD License

AUTHOR
  BaseX is primarily developed by Christian Gruen <cg@basex.org> with
  the help of various contributors <https://basex.org/about/open-source/>

  The man page was written by Alexander Holupirek <alex@holupirek.de> while packaging BaseX for Debian GNU/Linux.
